var steps = 512

var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(128 + 128 * (cos(frameCount * 0.01)))
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < steps; i++) {
    push()
    fill(128 + 128 * (cos(frameCount * 0.01 + i * 0.025)))
    noStroke()
    translate(windowWidth * 0.5 + (i - (steps * 0.5 + 0.5)) * sin(frameCount * 0.01 + i * 0.025) * boardSize * 0.001, windowHeight * 0.5 + (i - (steps * 0.5 + 0.5)) * boardSize * 0.00125)
    rotate(frameCount * 0.01 + i * 0.025)
    ellipse(0, 0, boardSize * 0.1, boardSize * 0.1 + boardSize * 0.2 * abs(sin(frameCount * 0.01 + i * 0.01)))
    pop()
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}
